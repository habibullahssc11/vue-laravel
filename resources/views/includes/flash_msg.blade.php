

@if( Session::has('alt_succ') )
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('alt_succ')  }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

@if( Session::has('alt_war') )
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ Session::get('alt_war')  }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif