@extends('layouts.app')

@section('content')
<div id="root" class="container" xmlns="http://www.w3.org/1999/html">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">
                    Product create
                    <a class="float-right" href="{{ route('products.create') }}">Add new</a>
                </div>



                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @include('includes.flash_msg')
                        {{--<div class="alert alert-primary alert-dismissible fade show" role="alert">
                            This is a primary alert—check it out!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                            This is a secondary alert—check it out!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            This is a warning alert—check it out!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            This is a info alert—check it out!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-light alert-dismissible fade show" role="alert">
                            This is a light alert—check it out!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="alert alert-dark alert-dismissible fade show" role="alert">
                            This is a dark alert—check it out!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>--}}

                        <form method="post" action="{{ route('products.update', $product['id'] ) }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" value="{{ $product['name'] }}" name="name" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" value="{{ $product['price'] }}" name="price" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" rows="6"> {{ $product['description'] }} </textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>

                        </form>

                </div>
            </div>
        </div>
    </div>
</div>



@endsection
