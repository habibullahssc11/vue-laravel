@extends('layouts.app')

@section('content')
    <div id="root" class="container" xmlns="http://www.w3.org/1999/html">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">
                        Product create
                        <a class="float-right" href="{{ route('products.create') }}">Add new</a>
                    </div>

                    <p v-for="product in products">adf asdf asdf</p>


                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @include('includes.flash_msg')
                        <form method="post" action="{{ route('products.store') }}">
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" name="price" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" rows="6"> </textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('vue/vue.js') }}"></script>
    <script src="{{ asset('axios/axios.js') }}"></script>
    <script type="text/javascript">var home_url="{{ url('/') }}"</script>

    <script type="text/javascript">
        var App = new Vue({
            el: "#root",
            data: {
                products:[]
            },

            mounted(){
                this.getAllProducts();
            },

            methods:{
                getAllProducts(){
                    //currentApp = this;
                    axios.get(home_url + '/all-products')
                        .then(response => {
                            App.products = response.data
                            console.log(App.products)
                        })
                }
            }

        })
    </script>
@endsection
