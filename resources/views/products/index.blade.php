@extends('layouts.app')

@section('content')

    <div id="root">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            Products
                            <button @click="showingAddSection = true;" class="float-right btn btn-primary" >Add new</button>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>#SL</td>
                                    <td>Name</td>
                                    <td>Price</td>
                                    <td>Action</td>
                                </tr>
                                </thead>

                                <tbody>

                                <tr v-for='( product, index ) in products'>
                                    <td> @{{ index+1 }}</td>
                                    <td> @{{ product.name }}</td>
                                    <td> @{{ product.price }}</td>
                                    <td>
                                        {{--<a href="{{ route('products.edit', $product['id']) }}">Edit</a>--}}

                                        <button class="btn btn-primary btn-sm" @click="showingEditSection = true; selectProduct(product)"><i class="fa fa-pencil-square-o"></i></button>
                                        <button class ="btn btn-danger btn-sm" @click="showingDeleteSection = true; selectProduct(product)"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card" id="addSec" v-if="showingAddSection">
                        <div class="card-header">
                            Product create
                            <button @click="showingAddSection = false;" type="button" class="close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="" v-model="newProduct.name" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" name="" v-model="newProduct.price" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="" v-model="newProduct.description"  rows="4"> </textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="button" @click="saveProduct" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>

                    <div class="card" id="editSec" v-if="showingEditSection">
                        <div class="card-header">
                            Product edit
                            <button @click="showingEditSection = false;" type="button" class="close">
                                <span>&times;</span>
                            </button>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="" v-model="clickedProduct.name" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" name="" v-model="clickedProduct.price" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="" v-model="clickedProduct.description"  rows="4"> </textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="button" @click="updateProduct" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </div>

                    <div class="card" id="deleteSec" v-if="showingDeleteSection">
                        <div class="card-header">
                            Product delete
                            <button type="button" class="close" @click="showingDeleteSection = false;">
                                <span>&times;</span>
                            </button>
                        </div>

                        <div class="card-body">
                            <h5>Are you sure delete @{{clickedProduct.name}}!</h5>

                            <div class="form-group text-right">
                                <button class="btn btn-primary" @click="showingDeleteSection = false;">No</button>
                                <button class="btn btn-danger" @click="showingDeleteSection = false; deleteProduct()">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>


<script src="{{ asset('vue/vue.js') }}"></script>
<script src="{{ asset('axios/axios.js') }}"></script>
<script type="text/javascript">var home_url="{{ url('/') }}"</script>

<script type="text/javascript">
    var App = new Vue({
        el: "#root",
        data: {
            showingAddSection: false,
            showingEditSection: false,
            showingDeleteSection: false,
            products:[],
            newProduct: {name: "", price: "", description: ""},
            clickedProduct: {}
        },

        mounted(){
            this.getAllProducts();
        },

        methods:{
            getAllProducts(){
                //currentApp = this;
                axios.get(home_url + '/all-products')
                    .then(response => {
                        App.products = response.data
                        //console.log(App.products)
                    })
            },

            saveProduct() {
                currentApp = this;
                axios.post(home_url + '/product/store', this.newProduct)
                    .then(response => {
                        console.log(response.data.status)
                        if( response.data.status == "success"){
                            this.getAllProducts();
                            currentApp.newProduct = {name: "", price: "", description: ""};
                        }
                    })
            },

            selectProduct( product ){
                currentApp = this;
                currentApp.clickedProduct = product;
            },
            updateProduct() {
                currentApp = this;
                axios.post(home_url + '/product/update/'+currentApp.clickedProduct.id, currentApp.clickedProduct)
                    .then(response => {
                        console.log(response.data.status);
                        if( response.data.status == "success"){
                            this.getAllProducts();
                            currentApp.clickedProduct = {name: "", price: "", description: ""};
                        }
                    })
            },

            deleteProduct() {
                currentApp = this;
                //alert(currentApp.clickedProduct.id);
                axios.post(home_url + '/product/destroy/'+currentApp.clickedProduct.id)
                    .then(response => {
                        console.log(response.data.status);
                        if( response.data.status == "success"){
                            this.getAllProducts();
                           
                        }
                    })
            }
        },




    })
</script>


@endsection
