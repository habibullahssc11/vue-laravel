<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Log;

class ProductsController extends Controller
{
    protected $Product;
    public function __construct( Product $Product )
    {
        $this->Product = $Product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save_product  = $this->Product->Create( $request->all() );

        if( $save_product ){
           return [
               'status' => 'success',
               'message'=>'Product has been successfully saved!'
           ];
        }else{
            return [
                'status' => 'fail',
                'message'=>'Product fail to save!'
            ];
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['product'] = $this->Product->where( 'id', $id )->first();
        return view('products.edit', $data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //Log::info( $request->all() );

        $product = $this->Product->where('id', $id)->first();
        $update =  $product->update( $request->all() );

        if( $update ){
            return [
                'status' => 'success',
                'message'=>'Product has been successfully update!'
            ];
        }else{
            return [
                'status' => 'fail',
                'message'=>'Product fail to update!'
            ];
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product =  $this->Product->where('id', $id)->first();
        $delete =  $product->delete();

        if( $delete ){
            return [
                'status' => 'success',
                'message'=>'Product has been successfully update!'
            ];
        }else{
            return [
                'status' => 'fail',
                'message'=>'Product fail to update!'
            ];
        }
    }

    function getAllProducts(){
        $products = $this->Product->orderBy('id', 'DESC' )->get()->toArray();

        return $products;
    }
}
